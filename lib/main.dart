import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_app/app_color.dart';
import 'package:my_app/page/product/characteristics.dart';
import 'package:my_app/page/product/main.dart';
import 'package:my_app/page/product/nutrition.dart';
import 'package:my_app/page/product/table.dart';

import 'app_icons.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: AppColors.white,
      ),
      home: ProductMainPage(),
      routes: {
        "product/main": (BuildContext context) {
          return ProductMainPage();
        },
        "product/characteristic": (BuildContext context) {
          return ProductCharacteristicsPage();
        },
        "product/nutrition": (BuildContext context) {
          return ProductNutritionPage();
        },
        "product/table": (BuildContext context) {
          return ProductTablePage();
        },
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          bottomOpacity: 0,
          shadowColor: new Color(0x00000000),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(
            "Mes scans",
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w500,
              color: AppColors.blue,
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 23, vertical: 0),
              child: Icon(
                AppIcons.barcode,
                color: AppColors.blue,
              ),
            )
          ],
        ),
        body: Container(
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            maxWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: Column(
            children: [
              DraggableScrollableSheet(
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  scrollController.addListener(() {
                    print("j'ai scroll");
                  });
                  return Container(
                    height: 0,
                  );
                },
              ),
              Expanded(
                  child: Align(
                      child: Transform.translate(
                          offset: Offset(-10, 0),
                          child: SvgPicture.asset("res/svg/ill_empty.svg")))),
              Column(
                children: [
                  Container(
                    height: 40,
                  ),
                  Text(
                    "Vous n'avez pas encore scanné de produit",
                    style: TextStyle(fontSize: 20, color: AppColors.blue),
                  ),
                  Container(
                    height: 40,
                  ),
                  TextButton(
                      style: OutlinedButton.styleFrom(
                        primary: AppColors.blue,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(22.0))),
                        backgroundColor: AppColors.yellow,
                      ),
                      onPressed: () {},
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                        child: Container(
                          constraints: BoxConstraints(
                            minHeight: 47,
                            minWidth: 160,
                            maxWidth: double.infinity,
                            maxHeight: 47,
                          ),
                          child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 0),
                                child: Text("COMMENCEZ",
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.blue,
                                    )),
                              ),
                              Icon(CupertinoIcons.arrow_right,
                                  color: AppColors.blue)
                            ],
                          ),
                        ),
                      )),
                  Container(
                    height: 120,
                  ),
                ],
              ),
            ],
          ),
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
