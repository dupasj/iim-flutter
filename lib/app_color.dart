import 'dart:ui';

class AppColors{
  static Color blue = Color(0xFF080040);
  static Color blueDark  = Color(0xFF050027);
  static Color yellow  = Color(0xFFFBAF02);
  static Color blueLight  = Color(0xFF41C7DF);
  static Color gray1    = Color(0xFFF6F6F8);
  static Color gray2     = Color(0xFFB8BBC6);
  static Color gray3      = Color(0xFF6A6A6A);
  static Color white       = Color(0xFFFFFFFF);
  static Color black        = Color(0xFF000000);
  static Color nutrientLevelLow        = Color(0xFF63993F);
  static Color nutrientKevelModerate        = Color(0xFF997B3F);
  static Color nutrientLevelHigh        = Color(0xFF993F3F);
  static Color nutriscoreA        = Color(0xFF008b4c);
  static Color nutriscoreB        = Color(0xFF80c142);
  static Color nutriscoreC        = Color(0xFFfeca0b);
  static Color nutriscoreD        = Color(0xFFf58220);
  static Color nutriscoreE        = Color(0xFFef3e22);
  static Color ecoScoreA        = Color(0xFF1E8F4E);
  static Color ecoScoreB        = Color(0xFF2ECC71);
  static Color ecoScoreC        = Color(0xFFFFC900);
  static Color ecoScoreD        = Color(0xFFEF7E1A);
  static Color ecoScoreE        = Color(0xFFE62D19);
}