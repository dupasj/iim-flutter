import 'package:flutter/cupertino.dart';
import 'package:my_app/widget/product_label_value.dart';

import '../app_color.dart';

class LabelValueLine extends LabelValue {
  LabelValueLine(
      {required String label, required String value, double fontSize = 20})
      : super(
          label: label,
          fontSize: fontSize,
          value: RichText(
            textAlign: TextAlign.left,
            softWrap: true,
            text: TextSpan(
                text: value,
                style: TextStyle(
                  color: AppColors.gray2,
                  fontSize: fontSize,
                )),
          ),
        ) {}
}
