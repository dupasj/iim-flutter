import 'package:flutter/cupertino.dart';

import '../app_color.dart';

class LabelValue extends Padding {
  LabelValue(
      {required String label, required Widget value, double fontSize = 20})
      : super(
            padding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 3),
                        child: RichText(
                          textAlign: TextAlign.left,
                          softWrap: true,
                          text: TextSpan(
                              text: label,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: AppColors.blue,
                                fontSize: fontSize,
                              )),
                        ),
                      )),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: value,
                  ),
                )
              ],
            )) {}
}
