import 'dart:math';

import 'package:flutter/cupertino.dart';

import '../app_color.dart';

class ProductHeader extends Stack {
  ProductHeader({required BuildContext context, child: Widget, header: Widget})
      : super(
          children: [
            header,
            ListView(
              children: [
                Container(height: 200),
                Container(
                  constraints: BoxConstraints(
                    maxWidth: max(MediaQuery.of(context).size.width, 800),
                    minWidth: 500,
                    minHeight: MediaQuery.of(context).size.height,
                  ),
                  decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 40,
                      left: 7,
                      right: 7,
                      bottom: 20,
                    ),
                    child: child,
                  ),
                )
              ],
            ),
          ],
        ) {}
}
