import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app/widget/product_header.dart';

import '../../app_color.dart';
import '../../app_icons.dart';

class ProductTablePage extends StatefulWidget {
  ProductTablePage({Key? key}) : super(key: key);

  @override
  ProductTablePageState createState() => ProductTablePageState();
}

class ProductTablePageState extends State<ProductTablePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 3,
        type: BottomNavigationBarType.fixed,
        backgroundColor: AppColors.white,
        selectedItemColor: AppColors.blueDark,
        unselectedItemColor: AppColors.gray3,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (value) {
          switch (value) {
            case 0:
              Navigator.pushNamed(context, "product/main");
              break;
            case 1:
              Navigator.pushNamed(context, "product/characteristic");
              break;
            case 2:
              Navigator.pushNamed(context, "product/nutrition");
              break;
            case 3:
              Navigator.pushNamed(context, "product/table");
              break;
          }
        },
        items: [
          BottomNavigationBarItem(
            title: Text('Fiche'),
            icon: Icon(AppIcons.tab_barcode),
          ),
          BottomNavigationBarItem(
            title: Text('Caractéristiques'),
            icon: Icon(AppIcons.tab_fridge),
          ),
          BottomNavigationBarItem(
            title: Text('Nutritions'),
            icon: Icon(AppIcons.tab_nutrition),
          ),
          BottomNavigationBarItem(
            title: Text('Tableaux'),
            icon: Icon(AppIcons.tab_array),
          ),
        ],
      ),
      body: ProductHeader(
        context: context,
        header: Container(
          height: 300,
          color: AppColors.black,
        ),
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Petits pois et carottes",
                        style: TextStyle(
                          color: AppColors.blueDark,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Cassegrain",
                        style: TextStyle(
                          color: AppColors.gray2,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                )),
            Container(
              height: 70,
            ),
            Table(
              columnWidths: {
                0: FlexColumnWidth(),
                1: IntrinsicColumnWidth(),
                2: IntrinsicColumnWidth(),
              },
              children: [
                TableRow(children: [
                  Container(),
                  HeaderText("Pour 100g"),
                  HeaderText("Par part")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Energie"),
                  HeaderTextValue("298 kj"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Matière grasse"),
                  HeaderTextValue("0,8 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Dont Acides gras saturés"),
                  HeaderTextValue("0,1 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Glucides"),
                  HeaderTextValue("298 kj"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Dont sucres"),
                  HeaderTextValue("5,2 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Fibres alimentaires"),
                  HeaderTextValue("5,2 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Protéines"),
                  HeaderTextValue("4,2 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Sel"),
                  HeaderTextValue("0,75 g"),
                  HeaderTextValue("?")
                ]),
                TableRow(children: [
                  HeaderTextLabel("Sodium"),
                  HeaderTextValue("0,295 g"),
                  HeaderTextValue("?")
                ])
              ],
            )
          ],
        ),
      ),
    );
  }
}

class HeaderTextValue extends Container {
  HeaderTextValue(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              alignment: Alignment.center,
              child: Text(
                title,
                style: TextStyle(
                    color: AppColors.blue,
                    fontWeight: FontWeight.w200,
                    fontSize: 16),
              )),
        )) {}
}

class HeaderTextLabel extends Container {
  HeaderTextLabel(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                title,
                style: TextStyle(
                    color: AppColors.blueDark,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              )),
        )) {}
}

class HeaderText extends Container {
  HeaderText(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              child: Text(
            title,
            style: TextStyle(
                color: AppColors.blueDark,
                fontWeight: FontWeight.bold,
                fontSize: 16),
          )),
        )) {}
}
