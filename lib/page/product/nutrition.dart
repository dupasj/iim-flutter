import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app/widget/product_header.dart';
import 'package:my_app/widget/product_label_value.dart';
import 'package:my_app/widget/product_label_value_line.dart';

import '../../app_color.dart';
import '../../app_icons.dart';

class ProductNutritionPage extends StatefulWidget {
  ProductNutritionPage({Key? key}) : super(key: key);

  @override
  ProductNutritionPageState createState() => ProductNutritionPageState();
}

class ProductNutritionPageState extends State<ProductNutritionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 2,
        type: BottomNavigationBarType.fixed,
        backgroundColor: AppColors.white,
        selectedItemColor: AppColors.blueDark,
        unselectedItemColor: AppColors.gray3,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (value) {
          switch (value) {
            case 0:
              Navigator.pushNamed(context, "product/main");
              break;
            case 1:
              Navigator.pushNamed(context, "product/characteristic");
              break;
            case 2:
              Navigator.pushNamed(context, "product/nutrition");
              break;
            case 3:
              Navigator.pushNamed(context, "product/table");
              break;
          }
        },
        items: [
          BottomNavigationBarItem(
            title: Text('Fiche'),
            icon: Icon(AppIcons.tab_barcode),
          ),
          BottomNavigationBarItem(
            title: Text('Caractéristiques'),
            icon: Icon(AppIcons.tab_fridge),
          ),
          BottomNavigationBarItem(
            title: Text('Nutritions'),
            icon: Icon(AppIcons.tab_nutrition),
          ),
          BottomNavigationBarItem(
            title: Text('Tableaux'),
            icon: Icon(AppIcons.tab_array),
          ),
        ],
      ),
      body: ProductHeader(
        context: context,
        header: Container(
          height: 300,
          color: AppColors.black,
        ),
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Petits pois et carottes",
                        style: TextStyle(
                          color: AppColors.blueDark,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Cassegrain",
                        style: TextStyle(
                          color: AppColors.gray2,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                )),
            Container(
              height: 70,
            ),
            LabelValueColor(
              label: "Matières grasses / lipide",
              value: "0.8g",
              intensity: Intensity.low,
            ),
            LabelValueColor(
              label: "Acides gras saturés",
              value: "0.8g",
              intensity: Intensity.low,
            ),
            LabelValueColor(
              label: "Sucres",
              value: "5.2g",
              intensity: Intensity.moderate,
            ),
            LabelValueColor(
              label: "Sel",
              value: "0.75g",
              intensity: Intensity.hight,
            ),
          ],
        ),
      ),
    );
  }
}

enum Intensity {
  low,
  moderate,
  hight,
}

// ignore: non_constant_identifier_names
Map<Intensity, Color> ColorIntensity = {
  Intensity.low: AppColors.nutrientLevelLow,
  Intensity.moderate: AppColors.nutrientKevelModerate,
  Intensity.hight: AppColors.nutrientLevelHigh,
};
Map<Intensity, String> MessageIntensity = {
  Intensity.low: "Faible quantitée",
  Intensity.moderate: "Quantité modérée",
  Intensity.hight: "Quantité élévée",
};

class LabelValueColor extends LabelValue {
  LabelValueColor({
    required String label,
    required Intensity intensity,
    required String value,
    double fontSize = 20,
  }) : super(
          label: label,
          fontSize: fontSize,
          value: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: RichText(
                  textAlign: TextAlign.right,
                  text: TextSpan(
                      text: value,
                      style: TextStyle(
                        color: ColorIntensity[intensity],
                        fontSize: fontSize,
                      )),
                ),
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: RichText(
                      textAlign: TextAlign.right,
                      text: TextSpan(
                        text: MessageIntensity[intensity],
                        style: TextStyle(
                          color: ColorIntensity[intensity],
                          fontSize: fontSize,
                        ),
                      ))),
            ],
          ),
        ) {}
}

class HeaderTextValue extends Container {
  HeaderTextValue(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              alignment: Alignment.center,
              child: Text(
                title,
                style: TextStyle(
                    color: AppColors.blue,
                    fontWeight: FontWeight.w200,
                    fontSize: 16),
              )),
        )) {}
}

class HeaderTextLabel extends Container {
  HeaderTextLabel(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                title,
                style: TextStyle(
                    color: AppColors.blueDark,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              )),
        )) {}
}

class HeaderText extends Container {
  HeaderText(String title)
      : super(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Align(
              child: Text(
            title,
            style: TextStyle(
                color: AppColors.blueDark,
                fontWeight: FontWeight.bold,
                fontSize: 16),
          )),
        )) {}
}
