
String foo({required String name,int number = 0,bool toUpperCase = false}) =>
    '${(toUpperCase ? name.toUpperCase() : name)} $number';

bool isPangramme  (String sentence) {
  List<String> latin = List<String>.of([
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'x',
    'y',
    'z'
  ]);

  for(String letter in latin){
    if (!sentence.toLowerCase().contains(letter.toLowerCase())){
      return false;
    }
  }

  return true;
}

int getScore (String sentence) {
    Map<List<String>,int> map = {
      ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T']: 1,
      ['D', 'G']: 2,
      ['B', 'C', 'M', 'P']: 3,
      ['F', 'H', 'V', 'W', 'Y']: 4,
      ['k']: 5,
      ['X','J']: 8,
      ['Q','Z']: 10,
    };

    int score = 0;
    map.forEach((List<String> letters, int value) {
      letters.forEach((String letter) {
        score += (sentence.toUpperCase().split(letter.toUpperCase()).length - 1) * value;
      });
    });

    return score;
}

void main(List<String> arguments) {
  print(isPangramme('Portez ce vieux whisky au juge blond qui fume'));
  print(isPangramme('coucou'));
  print(getScore('coucou'));
  print(getScore('Portez ce vieux whisky au juge blond qui fume'));

  int? num = null;

  print(num?.roundToDouble() ?? 'Vide');


  print(foo(name: 'a'));
  print(foo(name: 'b', number: 1));
  print(foo(name: 'c', toUpperCase: true));
  print(foo(name: 'd', number: 2, toUpperCase: true));

  List<int> collection = List<int>.of([25, 42, 79, 12],growable: false);

  collection.sort((int a,int b){
    return a - b;
  });
  collection.map((int a){
    return a * 2;
  });

  Iterable<int> pair = collection.where((int element) => element % 2 == 0);

  print(pair);
}

class Bike extends Vehicle {
  Bike() : super(1);
}

class Car extends Vehicle {
  Car () : super(4);
}

abstract class Vehicle {
  int _speed = 0;
  late int capacity;

  Vehicle(int capacity){
    this.capacity = capacity;
  }

  Vehicle factory(String type){
    switch (type) {
      case 'car':
        return Car();
      case 'bike':
        return Bike();
    }

    throw Error();
  }

  @override
  bool operator == (Object other) {
    if (other is Vehicle && this.toString() == other.toString()){
      return true;
    }

    return this.hashCode == other.hashCode;
  }

  int speed(){
    return this._speed;
  }

  Vehicle speedUp(int increment){
    this._speed += increment;

    return this;
  }

  @override
  String toString() {
    return '{speed: '+this.speed().toString()+',capacity:'+this.capacity.toString()+'}';
  }
}


